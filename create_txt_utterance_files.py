"""
Read the 'Intent_Classification_ABC_Bank_1.0.xlsx' file and
split the comma-delimited text into separate lines.

@author: Michael Kottas
"""

import pandas as pd
import os


# Set file paths.
excel_file = r'/Users/mkottas/Documents/2018_intro/test_exercises/TRAINING_Mini Project_v1.1/NLU Development/Intent Classification/Intent_Classification_ABC_Bank_1.0.xlsx'
output_path = r'/Users/mkottas/Documents/2018_intro/Advanced_Exercise/utterances'

# Read excel file.
data = pd.read_excel(excel_file, sheet_name = 'GENERAL_IVR', header=None, usecols='B:C', skiprows=3)

# Drop the NaN values
data = data.dropna(axis=0, how='any').reset_index()

# Iterate through the intents.
split_text = []
lines = 0
for index, text in enumerate(data[1]):
    
    # Split the string by delimiter.
    split_text.append(text.replace(', ', ',').split(','))
    
    # Count the number of lines.
    lines += len(split_text)
    
    # Create txt file.
    filename = "{0:02d}_".format(index) + data[0][index].replace(r'/', '-') + '.txt'
    txt_file = os.path.join(output_path, filename)
    with open(txt_file, 'w') as f:
        for item in split_text[index]:
            f.write("%s\n" % item)
    f.close()

print('Number of utterances: {}'.format(lines))